import exifread
from datetime import datetime
import os
import sys


def get_image_date(filename):
    '''
    Gets the original datetime from the EXIF metadata.
    '''
    with open(filename, 'rb') as f:
        tags = exifread.process_file(f)
        if 'EXIF DateTimeOriginal' in tags:
            datestr = str(tags['EXIF DateTimeOriginal'])
            return datetime.strptime(datestr, '%Y:%m:%d %H:%M:%S')
        else:
            raise ValueError('cannot get image datetime')


ifilenames = sys.argv[1:]
for ifilename in ifilenames:
    datestr = None
    try:
        date = get_image_date(ifilename)
        datestr = date.strftime('%Y-%m-%d_%H-%M-%S')
    except ValueError:
        datestr = "unknown-date"
    i = 0
    ofilename = '{}-{:03d}.jpg'.format(datestr, i)
    while os.path.exists(ofilename):
        i += 1
        ofilename = '{}-{:03d}.jpg'.format(datestr, i)
    print('"{}" -> "{}"'.format(ifilename, ofilename))
    os.rename(ifilename, ofilename)
